def test_fail2ban_installed(host):
    fail2ban_package = host.package('fail2ban')

    assert fail2ban_package.is_installed


def test_fail2ban_is_running(host):
    fail2ban_service = host.service('fail2ban')

    assert fail2ban_service.is_running
    assert fail2ban_service.is_enabled
