[![pipeline status](https://git.coop/aptivate/ansible-roles/fail2ban/badges/master/pipeline.svg)](https://git.coop/aptivate/ansible-roles/fail2ban/commits/master)

# fail2ban

A role to install and configure fail2ban.

# Requirements

None.

# Role Variables

## Mandatory

* `fail2bain_jail_local`: Local path to fail2ban `jail.local` configuration.

## Optional

* `fail2ban_jail_specs`: Local paths to fail2ban jail configurations.
  * Defaults to `[]`. Only needed when using custom jails.

# Dependencies

None.

# Example Playbook

```yaml
- hosts: localhost
  roles:
     - role: fail2ban
      fail2ban_jail_local: templates/fail2ban/jail.local
      fail2ban_jail-specs:
        - templates/fail2ban/ssh.conf
        - templates/fail2ban/nginx-noscript.conf
```

# Testing

We use Linode for our testing here.

You'll need to expose the `LINODE_API_KEY` environment variable for that.

```
$ pipenv install --dev
$ pipenv run molecule test
```

# License

  * https://www.gnu.org/licenses/gpl-3.0.en.html

# Author Information

  * https://aptivate.org/
  * https://git.coop/aptivate
